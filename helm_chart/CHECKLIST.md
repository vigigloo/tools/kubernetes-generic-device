# Checklist

To reach quality goal, please complete this checklist, and remove this file when everything is done.
It's OK to commit and deliver while all points are not done, but keep this file to know what should be improved.

- Review and Update the Chart.yaml File
    - [x] Check and update metadata including `name`, `version`, `appVersion`, `description`.
- Customize Values.yaml
    - [x] Add/Change default values for configurations.
    - [x] Document default resource CPU requests and memory limits.
    - [x] Update image repository and tag.
    - [x] Document values.yaml to support [readme-generator-for-helm](https://github.com/bitnami-labs/readme-generator-for-helm).
- Update the Templates
    - [x] Remove unused templates in `templates/`.
    - [x] Check that no sensitive endpoint is accessible.
    - [x] Verify the service type and ports.
    - [x] Add liveness and readiness probes in the deployment template.
    - [x] Specify resource requests and limits in the deployment template to ensure efficient resource usage.
    - [x] Make sure resources are properly annotated and labeled for better management and observability.
- Quality
    - [x] Add Helm tests in `templates/tests/` to verify the chart's functionality.
    - [x] Use `helm lint` to check for issues with the chart structure and syntax.
    - [x] Create a README.md explaining the chart's purpose, configuration options, and any prerequisites.
    - [x] Remove unnecessary comments (generated for templating purpose)
    - [x] Customize `templates/NOTES.txt` to display useful information post-installation to devOps friends.
    - [x] Review `LICENSE`.
- Test
    - [x] Use `helm install --dry-run --debug` and/or `helm template .` to test the rendering of your templates.
    - [x] Deploy the chart in a test environment to ensure it works as expected.
- Deliver
    - [x] Use `helm package` locally to ensure that chart can be packaged.
    - [x] Configure CI to build automatically the package.
