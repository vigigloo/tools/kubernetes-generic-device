# Helm chart **generic-device-plugin**

The `generic-device-plugin` is a Kubernetes device plugin designed to allocate generic Linux devices to Kubernetes Pods. 
It supports devices that don't require special drivers, like serial devices, the FUSE device, or video cameras. 
This plugin facilitates various use cases, such as accessing video and sound devices, running IoT applications, 
and mounting FUSE filesystems without needing `privileged` access.

### Overview
The plugin can be configured to discover and allocate any desired device using the `--device` flag. 
For example, to advertise all video devices to the cluster, you could use a command 
like `--device='{"name": "video", "groups": [{"paths": [{"path": "/dev/video0"}]}]}'`. 
Pods requiring a video capture device can request it through the Kubernetes Pod `resources` field.

## Parameters

### Global parameters

| Name                         | Description                                                                                                                                                    | Value                  |
| ---------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- |
| `nameOverride`               | represents the name that should override the default naming of the application                                                                                 | `""`                   |
| `fullnameOverride`           | represents the full name that should override the default full naming of the application                                                                       | `""`                   |
| `nodeSelector`               | is a dictionary that adds scheduling constraints to the pods of the application. These constraints can specify the properties of the desired host of the pods. | `{}`                   |
| `tolerations`                | A list of tolerations that can be applied to Pods of the application. Each entry should represent a separate toleration.                                       |                        |
| `securityContext`            | Security context applied to daemon set                                                                                                                         |                        |
| `securityContext.privileged` | Should daemon set being privileged ?                                                                                                                           | `true`                 |
| `affinity`                   | The `affinity` parameter is an object that sets the scheduling constraints on the pods of the application by setting affinity rules. Affinity rules            | `{}`                   |
| `podAnnotations`             | provides additional metadata for pods in the form of annotations.                                                                                              | `{}`                   |
| `priorityClassName`          | A PriorityClass is a non-namespaced object that defines a mapping from a priority class name to the integer value of the priority.                             | `system-node-critical` |
| `devices`                    | A list of Devices for the pod                                                                                                                                  | `[]`                   |

### OCI parameters

| Name                          | Description                                                                                   | Value                         |
| ----------------------------- | --------------------------------------------------------------------------------------------- | ----------------------------- |
| `image.repository`            | denotes the name of the image repository, in this case 'nginx'                                | `squat/generic-device-plugin` |
| `image.pullPolicy`            | a Kubernetes directive which describes how to fetch the Docker image                          | `Always`                      |
| `image.credentials.name:`     | A string representing the name of these credentials, which can be any string.                 |                               |
| `image.credentials.registry:` | The string value representing the domain of the image registry. In this example, "gitlab.com" |                               |
| `image.credentials.username:` | The string value representing the username for authentication at the mentioned registry       |                               |
| `image.credentials.password:` | The string value representing the password for the mentioned username at the registry         |                               |
| `image.tag`                   | used to reference a specific image snapshot in the image repository.                          | `latest`                      |

### Service parameters

This section defines parameters for the service which exposes the application to the network.

| Name                 | Description                                                                               | Value  |
| -------------------- | ----------------------------------------------------------------------------------------- | ------ |
| `service.targetPort` | The port of the target Pods where the HTTP service is running. Here, it's also set to 80. | `8080` |

### Probe parameters

This section defines liveness, readiness, and startup probes for Kubernetes.
Probes allow Kubernetes to check for certain conditions and respond accordingly.

| Name               | Description                                                                                     | Value |
| ------------------ | ----------------------------------------------------------------------------------------------- | ----- |
| `probes.liveness`  | The kubelet uses liveness probes to know when to restart a container.                           |       |
| `probes.readiness` | The kubelet uses readiness probes to know when a container is ready to start accepting traffic. | `nil` |
| `probes.startup`   | The kubelet uses startup probes to know when a container application has started.               |       |

### Resource parameters

This configuration block defines the resources requests and limits for the pods in the deployment.
Kubernetes uses requests for scheduling pods on its nodes, and to ensure fair resource sharing between pods.
Limits come into play to guarantee a maximum amount of a resource that a pod can consume, preventing it affecting other pods adversely.
This block has two main components: `limits` and `requests`, each for CPU and memory.

| Name                         | Description                                                                                                                                            | Value |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | ----- |
| `resources.limits.cpu:`      | The maximum amount of CPU a pod can use. If the CPU use exceeds this limit,                                                                            |       |
| `resources.limits.memory:`   | The maximum amount of memory a pod can use. If a pod exceeds this limit,                                                                               |       |
| `resources.requests.cpu:`    | The amount of CPU a pod requests upon scheduling. Kubernetes guarantees to allocate this amount of CPU. '100m' implies 0.1 of a CPU core.              |       |
| `resources.requests.memory:` | The amount of memory a pod requests upon scheduling. Kubernetes guarantees to allocate this amount of memory. '128Mi' implies 128 Mebibytes of memory. |       |
